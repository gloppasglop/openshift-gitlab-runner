FROM centos/s2i-core-centos7

ADD https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 $APP_ROOT/bin/gitlab-runner

COPY ./s2i/bin/ $STI_SCRIPTS_PATH

COPY ./root /

CMD $STI_SCRIPTS_PATH/usage

